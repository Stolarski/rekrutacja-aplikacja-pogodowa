#!/usr/bin/env bash

echo PHP spec tests started...
bin/phpspec run --format=pretty
echo PHP spec tests ended
