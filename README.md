**TESTOWE URUCHOMIENIE PROJEKTU**

Krok 1 - zainstalować biblioteki:

````
composer install
````

**Krok 2 - podłączanie bazy daynch**

Na potrzeby testu w pliku .env są wpisane na stałe parametry testowej bazy danych.
Nie trzeba się nią przejmować, wystarczy uruchomić migrację:

````
php bin/console doctrine:migrations:migrate -n
````

W razie gdyby "zahardcodowana" baza nie działa, należy ustanowić połączenie z inną pustą bazą danych i uruchomić migrację.


**Krok 3 - postawić serwer symfony na wolnym hoście, np:**

````
php bin/console server:run 127.0.0.1:8027 --env=dev
````


**Krok 4 - oczywiście odwiedzić adres**

````
http://localhost:8027/
````

**DODATKOWE INFORMACJE**

panel administracyjny znajduje się pod adresem
````
/admin
````
nie wymaga żadnego hasła (oczywiście w normalnej sytuacji powinien być zabezpieczony, np. domenowo)

