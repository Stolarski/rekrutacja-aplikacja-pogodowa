<?php

namespace App\Enum;


class WeatherServiceEnum
{
    const DARK_SKY = 'DARK_SKY';
    const OPEN_WEATHER_MAP = 'OPEN_WEATHER_MAP';
}
