<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @param ORM
 * @param Assert
 * @ORM\Table(name="weather")
 * @ORM\Entity(repositoryClass="App\Repository\WeatherRepository")
 */
class Weather
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $downloadDate;

    /**
     * @ORM\Column(type="integer", length=4, nullable=false)
     */
    private $temperature;

    /**
     * @ORM\Column(type="integer", length=4, nullable=false)
     */
    private $humidity;

    /**
     * @ORM\Column(type="integer", length=4, nullable=false)
     */
    private $windSpeed;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDownloadDate(): DateTime
    {
        return $this->downloadDate;
    }

    public function setDownloadDate($downloadDate)
    {
        $this->downloadDate = $downloadDate;

        return $this;
    }

    public function getTemperature(): int
    {
        return $this->temperature;
    }

    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getHumidity(): int
    {
        return $this->humidity;
    }

    public function setHumidity($humidity)
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getWindSpeed(): int
    {
        return $this->windSpeed;
    }

    public function setWindSpeed($windSpeed)
    {
        $this->windSpeed = $windSpeed;

        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city)
    {
        $this->city = $city;

        return $this;
    }

    public function toArray()
    {
        return [
            'downloadDate' => (string)$this->getDownloadDate()->format('Y-m-d H:i:s'),
            'temperature' => $this->temperature,
            'humidity' => $this->humidity,
            'windSpeed' => $this->windSpeed
        ];
    }
}
