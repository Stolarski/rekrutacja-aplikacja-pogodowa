<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @param ORM
 * @param Assert
 * @ORM\Table(name="weather_service_config")
 * @ORM\Entity
 */
class WeatherServiceConfig
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", length=1, nullable=false)
     */
    private $isUsed;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function isUsed(): bool
    {
        return $this->isUsed;
    }

    public function setIsUsed(bool $isUsed)
    {
        $this->isUsed = $isUsed;
    }
}
