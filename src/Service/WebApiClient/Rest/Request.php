<?php

namespace App\Service\WebApiClient\Rest;

class Request
{
    public $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }
}
