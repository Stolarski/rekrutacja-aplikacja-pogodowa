<?php

namespace App\Service\WebApiClient\Rest;

use GuzzleHttp\Client;

class RestClient
{
    public function makeGetHttpRequest(Request $request): array
    {
        $client = new Client([
            'timeout' => 6.0,
        ]);

        $response = $client->request('GET', $request->getUrl());

        return json_decode($response->getBody()->getContents(), true);
    }
}
