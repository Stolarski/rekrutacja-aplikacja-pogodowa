<?php

namespace App\Service\WeatherProvider;

use App\Enum\WeatherServiceEnum;
use App\Service\WeatherProvider\Includes\WeatherServiceAbstract;
use App\Service\WeatherProvider\Integration\DarkSky\DarkSkyWeatherService;
use App\Service\WeatherProvider\Integration\OpenWeatherMap\OpenWeatherMapWeatherService;
use App\Service\WebApiClient\Rest\RestClient;
use InvalidArgumentException;

class WeatherServiceFactory
{
    /**
     * @var RestClient
     */
    private $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function getWeatherService(string $weatherServiceName): WeatherServiceAbstract
    {
        $weatherService = null;

        switch ($weatherServiceName) {
            case WeatherServiceEnum::DARK_SKY:
                $weatherService = new DarkSkyWeatherService($this->restClient);
                break;
            case WeatherServiceEnum::OPEN_WEATHER_MAP:
                $weatherService = new OpenWeatherMapWeatherService($this->restClient);
                break;
        }

        if (!$weatherService instanceof WeatherServiceAbstract) {
            throw new InvalidArgumentException("Weather service {$weatherServiceName} invalid.");
        }

        return $weatherService;
    }
}
