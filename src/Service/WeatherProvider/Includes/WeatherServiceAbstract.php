<?php

namespace App\Service\WeatherProvider\Includes;

use App\Entity\City;
use App\Entity\Weather;
use App\Service\WebApiClient\Rest\RestClient;

abstract class WeatherServiceAbstract
{
    /**
     * @var RestClient
     */
    private $restClient;

    /**
     * @var RequestBuilderInterface
     */
    private $requestBuilder;

    /**
     * @var ResponseMapperInterface
     */
    private $responseMapper;

    public function __construct(
        RestClient $restClient,
        RequestBuilderInterface $requestBuilder,
        ResponseMapperInterface $responseMapper
    )
    {
        $this->restClient = $restClient;
        $this->requestBuilder = $requestBuilder;
        $this->responseMapper = $responseMapper;
    }

    public function downloadWeather(City $city): Weather
    {
        $response = $this->restClient->makeGetHttpRequest($this->requestBuilder->buildRequest($city));

        return $this->responseMapper->mapResponse($response, $city);
    }
}
