<?php

namespace App\Service\WeatherProvider\Includes;

use App\Entity\City;
use App\Entity\Weather;

interface ResponseMapperInterface
{
    public function mapResponse($response, City $city): Weather;
}
