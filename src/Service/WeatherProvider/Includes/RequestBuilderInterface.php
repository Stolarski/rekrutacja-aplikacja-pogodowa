<?php

namespace App\Service\WeatherProvider\Includes;

use App\Entity\City;
use App\Service\WebApiClient\Rest\Request;

interface RequestBuilderInterface
{
    public function buildRequest(City $city): Request;
}
