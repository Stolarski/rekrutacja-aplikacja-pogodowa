<?php

namespace App\Service\WeatherProvider\Integration\OpenWeatherMap;

use App\Service\WeatherProvider\Includes\WeatherServiceAbstract;
use App\Service\WebApiClient\Rest\RestClient;

class OpenWeatherMapWeatherService extends WeatherServiceAbstract
{
    public function __construct(RestClient $restClient)
    {
        parent::__construct(
            $restClient,
            new OpenWeatherMapRequestBuilder(),
            new OpenWeatherMapResponseMapper()
        );
    }
}
