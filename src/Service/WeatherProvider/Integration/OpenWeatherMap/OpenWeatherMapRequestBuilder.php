<?php

namespace App\Service\WeatherProvider\Integration\OpenWeatherMap;

use App\Entity\City;
use App\Service\WeatherProvider\Includes\RequestBuilderInterface;
use App\Service\WebApiClient\Rest\Request;

class OpenWeatherMapRequestBuilder implements RequestBuilderInterface
{
    const URI = 'api.openweathermap.org/data/2.5/weather';
    const APP_ID = '8d01136800f5bf949b288aa7ec33b71c';

    public function buildRequest(City $city): Request
    {
        return new Request($this->prepareUrl($city));
    }

    private function prepareUrl(City $city)
    {
        $query = http_build_query([
            'q' => $city->getName(),
            'APPID' => self::APP_ID,
            'units' => 'metric',
        ]);

        return self::URI . '?' . $query;
    }
}
