<?php

namespace App\Service\WeatherProvider\Integration\OpenWeatherMap;

use App\Entity\City;
use App\Entity\Weather;
use App\Service\WeatherProvider\Includes\ResponseMapperInterface;
use DateTime;

class OpenWeatherMapResponseMapper implements ResponseMapperInterface
{
    public function mapResponse($response, City $city): Weather
    {
        $weather = new Weather();

        return $weather
            ->setDownloadDate(new DateTime())
            ->setHumidity($response['main']['humidity'])
            ->setTemperature($response['main']['temp'])
            ->setWindSpeed($response['wind']['speed'])
            ->setCity($city);
    }
}
