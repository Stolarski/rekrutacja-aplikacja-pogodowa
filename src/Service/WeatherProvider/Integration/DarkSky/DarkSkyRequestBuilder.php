<?php

namespace App\Service\WeatherProvider\Integration\DarkSky;

use App\Entity\City;
use App\Service\WeatherProvider\Includes\RequestBuilderInterface;
use App\Service\WebApiClient\Rest\Request;

class DarkSkyRequestBuilder implements RequestBuilderInterface
{
    const URI = 'https://api.darksky.net/forecast';
    const APP_ID = '36216bd58df29ab9e7084f8de327da71';

    public function buildRequest(City $city): Request
    {
        return new Request($this->prepareUrl($city));
    }

    private function prepareUrl(City $city)
    {
        return self::URI . '/' . self::APP_ID . '/' . $city->getLatitude() . ',' . $city->getLongitude() . '?units=si';
    }
}
