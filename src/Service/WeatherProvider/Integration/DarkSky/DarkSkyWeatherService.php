<?php

namespace App\Service\WeatherProvider\Integration\DarkSky;

use App\Service\WeatherProvider\Includes\WeatherServiceAbstract;
use App\Service\WebApiClient\Rest\RestClient;

class DarkSkyWeatherService extends WeatherServiceAbstract
{
    public function __construct(RestClient $restClient)
    {
        parent::__construct(
            $restClient,
            new DarkSkyRequestBuilder(),
            new DarkSkyResponseMapper()
        );
    }
}
