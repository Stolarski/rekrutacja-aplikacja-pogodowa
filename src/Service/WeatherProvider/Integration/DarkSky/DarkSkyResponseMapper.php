<?php

namespace App\Service\WeatherProvider\Integration\DarkSky;

use App\Entity\City;
use App\Entity\Weather;
use App\Service\WeatherProvider\Includes\ResponseMapperInterface;
use DateTime;

class DarkSkyResponseMapper implements ResponseMapperInterface
{
    public function mapResponse($response, City $city): Weather
    {
        $weather = new Weather();

        return $weather
            ->setDownloadDate(new DateTime())
            ->setHumidity($response['currently']['humidity'] * 100)
            ->setTemperature($response['currently']['temperature'])
            ->setWindSpeed($response['currently']['windSpeed'])
            ->setCity($city);
    }
}
