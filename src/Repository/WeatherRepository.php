<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Weather;
use Doctrine\ORM\EntityRepository;

class WeatherRepository extends EntityRepository
{
    public function getLastWeatherForCity(City $city): ?Weather
    {
        return $this->createQueryBuilder('w')
            ->select()
            ->where('w.city = :cityId')
            ->setParameter('cityId', $city->getId())
            ->setMaxResults(1)
            ->orderBy('w.downloadDate', 'DESC')
            ->getQuery()
            ->getSingleResult();
    }
}
