<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190114053556 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `city` (`id`, `name`, `latitude`, `longitude`)
VALUES
	(1, 'Tarnów', 50.0121, 20.985842),
	(2, 'Sosnowiec', 50.286263, 19.104078),
	(3, 'Poznań', 52.406376, 16.925167),
	(4, 'Toruń', 53.01379, 18.598444),
	(5, 'Kraków', 50.049683, 19.944544);
");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("TRUNCATE TABLE city");
    }
}
