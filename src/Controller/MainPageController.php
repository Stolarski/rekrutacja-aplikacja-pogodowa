<?php

namespace App\Controller;

use App\Entity\City;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainPageController extends AbstractController
{
    public function index()
    {
        $cities = $this->getDoctrine()
            ->getRepository(City::class)
            ->findAll();

        return $this->render('main_page/index.html.twig', ['cities' => $cities]);
    }
}
