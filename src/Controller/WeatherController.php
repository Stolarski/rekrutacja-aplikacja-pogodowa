<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Weather;
use App\Entity\WeatherServiceConfig;
use App\Repository\WeatherRepository;
use App\Service\WeatherProvider\WeatherServiceFactory;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class WeatherController extends AbstractController
{
    public function getWeather($cityId, WeatherServiceFactory $weatherServiceFactory)
    {
        /** @var City $city */
        $city = $this->getDoctrine()
            ->getRepository(City::class)
            ->findOneBy(['id' => $cityId]);

        /** @var WeatherServiceConfig $weatherServiceConfig */
        $weatherServiceConfig = $this->getDoctrine()
            ->getRepository(WeatherServiceConfig::class)
            ->findOneBy(['isUsed' => true]);

        if (!empty($city) && $city instanceof City) {
            try {
                //Download weather from weather service
                $weatherService = $weatherServiceFactory->getWeatherService($weatherServiceConfig->getName());
                $weather = $weatherService->downloadWeather($city);
                $doctrineManager = $this->getDoctrine()->getManager();
                $doctrineManager->persist($weather);
                $doctrineManager->flush();
            } catch (\Exception $exception) {
                //Get weather from database if downloading weather failed from any reason
                /** @var WeatherRepository $weatherRepository */
                $weatherRepository = $this->getDoctrine()
                    ->getRepository(Weather::class);

                $weather = $weatherRepository->getLastWeatherForCity($city);
            }

            if (!empty($weather)) {
                return new JsonResponse($weather->toArray(), Response::HTTP_OK);
            }
        }

        throw new InvalidArgumentException('Weather information could not be delivered.');
    }
}
