$('#city-select').on('change', function () {
    refreshBlock();
});

setInterval(function () {
    refreshBlock();
}, 60 * 1000);

function refreshBlock() {
    var selectedOption = $('#city-select').find('option:selected');
    var selectedOptionValue = selectedOption.val();

    if (selectedOptionValue) {

        $('#weather-block').hide();
        $('.loader').show();

        $.ajax(
            {
                url: "/weather/" + selectedOptionValue,
                method: 'GET',
                dataType: "json",
                success: function (result) {
                    $('#city-name').text(selectedOption.text());
                    $('#wind-speed').text(result.windSpeed);
                    $('#humidity').text(result.humidity);
                    $('#temperature').text(result.temperature);
                    $('#refresh-date').text(result.downloadDate);

                    $('#weather-block').show();
                    $('.loader').hide();
                }
            });
    }
}
